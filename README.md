# Pi-Hole

###### What is this?

This is a repo containing blocklists to achieve various things. From blocking windows updates, to blocking windows telemetry or alternatively blocking a whole lot of ad servers, this repo has lists for it.



###### About Bing Chat inside of the Microsoft Edge browser

If you want Bing Chat to work but don't care about anything else bing-related you can import every adlist on here or just "all-in-one.txt", "block-windows-updates.txt" and "schizophrenia.txt" and then manually whitelist "edgeservices.bing.com". Depending on what other adlists you have besides mine, you may need to whitelist "sydney.bing.com" too. Once you whitelist those 2 domains you can block everything else bing-related (yes, even "www.bing.com"). BUT beware that you will need to use a different search engines inside Edge if you want to browse the web using it.



###### all-in-one.txt

Blocklist combining every other blocklist on this repo, EXCEPT "schizophrenia.txt" and "block-windows-updates.txt" since "schizophrenia.txt" WILL brick your Windows system, or at least certain parts of it. "block-windows-updates.txt" isn't included because I want that to be a adlist you add and can afterwards enable/disable at will to let windows update itself and block it afterwards again.



###### blocklist.txt

This is the old original blocklist this repo started with. It largely just contains a few windows telemetry domains and domains featured on [d3wards'](https://d3ward.github.io/toolz/adblock.html) adblock test.



###### block-windows-updates.txt

Pretty self explanatory. Blocks domains used to update Windows, as well as check for updates. Intended to be used as a adlist you add and enable/disable at will depending on when you have time for windows to update.



###### chatgpt-generated.txt

This is a telemetry list for both windows telemetry, as well as Bing/Edge Telemetry, which was conducted by the GPT-4 Browsing Beta Model.



###### experimental.txt

Largely just testing around with certain domains and what exactly happens if you block them.
